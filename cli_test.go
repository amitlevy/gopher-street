package main

import (
	"bytes"
	"context"
	"os"
	"path/filepath"
	"testing"

	"github.com/spf13/cobra"
	"gitlab.com/amitlevy/gopher-street/db"
	"gitlab.com/amitlevy/gopher-street/helpers"
)

var configFixtures = filepath.Join("config", "fixtures", "configs")
var goodConfig = filepath.Join(configFixtures, "config.yml")
var readerFixtures = filepath.Join("read", helpers.CSVTransactionsPath)
var goodDataFile = filepath.Join(readerFixtures, "data.csv")

func newCmdBuffer(cmd *cobra.Command) *bytes.Buffer {
	b := new(bytes.Buffer)
	cmd.SetOut(b)
	cmd.SetErr(b)
	return b
}

func TestRootCmd(t *testing.T) {
	err := rootCmd.Execute()
	helpers.FailTestIfErr(t, err)
}

func TestNoSuchCmd(t *testing.T) {
	err := CLIInit(goodConfig)
	helpers.FailTestIfErr(t, err)
	rootCmd.SetArgs([]string{"blah"})
	err = rootCmd.Execute()
	helpers.ExpectContains(t, err.Error(), "unknown command")
}

func TestLoadCmdMissingFilePath(t *testing.T) {
	err := CLIInit(goodConfig)
	helpers.FailTestIfErr(t, err)
	rootCmd.SetArgs([]string{"load"})
	err = rootCmd.Execute()
	helpers.ExpectContains(t, err.Error(), "path missing")
}

func TestLoadCmdFilePathNotExist(t *testing.T) {
	err := CLIInit(goodConfig)
	helpers.FailTestIfErr(t, err)
	rootCmd.SetArgs([]string{"load", "not_exist"})
	err = rootCmd.Execute()
	helpers.ExpectContains(t, err.Error(), "No such file")
}

func TestLoadCmdPathNotFile(t *testing.T) {
	err := CLIInit(goodConfig)
	helpers.FailTestIfErr(t, err)
	rootCmd.SetArgs([]string{"load", "config"})
	err = rootCmd.Execute()
	helpers.ExpectContains(t, err.Error(), "is not file")
}

func TestLoadOutOfRangeMapper(t *testing.T) {
	err := CLIInit(filepath.Join(configFixtures, "out-of-range.yml"))
	helpers.FailTestIfErr(t, err)
	rootCmd.SetArgs([]string{"load", goodDataFile})
	err = rootCmd.Execute()
	helpers.ExpectError(t, err)
}

func TestLoadCmd(t *testing.T) {
	err := CLIInit(goodConfig)
	helpers.FailTestIfErr(t, err)
	b := newCmdBuffer(rootCmd)
	rootCmd.SetArgs([]string{"load", goodDataFile})
	err = rootCmd.Execute()
	helpers.FailTestIfErr(t, err)
	helpers.ExpectContains(t, b.String(), "eating outside")
}

func TestGetEmptyExpenses(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	_db := db.Instance(ctx, helpers.MongoURI)
	helpers.FailTestIfErr(t, _db.DropDB(ctx))
	err := CLIInit(goodConfig)
	helpers.FailTestIfErr(t, err)
	rootCmd.SetArgs([]string{"get"})
	err = rootCmd.Execute()
	helpers.FailTestIfErr(t, err)
}

func TestGetExpenses(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	_db := db.Instance(ctx, helpers.MongoURI)
	defer helpers.FailTestIfErr(t, _db.DropDB(ctx))
	err := CLIInit(goodConfig)
	helpers.FailTestIfErr(t, err)
	rootCmd.SetArgs([]string{"load", goodDataFile})
	err = rootCmd.Execute()
	helpers.FailTestIfErr(t, err)
	rootCmd.SetArgs([]string{"get"})
	err = rootCmd.Execute()
	helpers.FailTestIfErr(t, err)
}

func TestExportExpensesDefaultFlags(t *testing.T) {
	err := CLIInit(goodConfig)
	helpers.FailTestIfErr(t, err)
	rootCmd.SetArgs([]string{"export"})
	err = rootCmd.Execute()
	helpers.FailTestIfErr(t, err)
	_, err = os.Stat("exported.csv")
	helpers.FailTestIfErr(t, err)
	os.Remove("exported.csv")
}

func TestExportExpensesCustomFlags(t *testing.T) {
	err := CLIInit(goodConfig)
	helpers.FailTestIfErr(t, err)
	rootCmd.SetArgs([]string{"export", "--format", "xlsx", "--path", "custom_name.xlsx"})
	err = rootCmd.Execute()
	helpers.FailTestIfErr(t, err)
	_, err = os.Stat("custom_name.xlsx")
	helpers.FailTestIfErr(t, err)
	os.Remove("custom_name.xlsx")
}

func TestExportExpensesUnsupportedFormat(t *testing.T) {
	err := CLIInit(goodConfig)
	helpers.FailTestIfErr(t, err)
	rootCmd.SetArgs([]string{"export", "--format", "hello", "--path", "custom_name2"})
	err = rootCmd.Execute()
	helpers.ExpectError(t, err)
	_, err = os.Stat("custom_name.xlsx")
	helpers.ExpectError(t, err)
}

func TestCLIInit(t *testing.T) {
	err := CLIInit(goodConfig)
	helpers.FailTestIfErr(t, err)
}
