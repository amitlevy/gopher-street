package db

import (
	"context"
	"fmt"
	"testing"
	"time"

	"gitlab.com/amitlevy/gopher-street/group"
	"gitlab.com/amitlevy/gopher-street/helpers"
)

type BadDecoderCursor struct{}

type EndDecodeErrCursor struct {
	BadDecoderCursor
}

func (f *BadDecoderCursor) Next(_ context.Context) bool {
	return true
}

func (f *BadDecoderCursor) Decode(_ interface{}) error {
	return fmt.Errorf("always fail")
}

func (f *BadDecoderCursor) Err() error {
	return nil
}

func (f *BadDecoderCursor) Close(_ context.Context) error {
	return fmt.Errorf("always fail")
}

func (f *EndDecodeErrCursor) Next(_ context.Context) bool {
	return false
}

func (f *EndDecodeErrCursor) Decode(_ interface{}) error {
	return nil
}

func (f *EndDecodeErrCursor) Err() error {
	return fmt.Errorf("always fail")
}

func TestBadURI(t *testing.T) {
	defer func() { _ = recover() }()
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	openDB(ctx, "")
	t.Errorf("did not panic")
}

func TestBadPing(t *testing.T) {
	defer func() { _ = recover() }()
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Microsecond)
	defer cancel()
	openDB(ctx, "mongodb://hangup")
	t.Errorf("did not panic")
}

func TestClosedTooEarly(t *testing.T) {
	defer func() { _ = recover() }()
	ctx, cancel := context.WithCancel(context.Background())
	db := Instance(ctx, helpers.MongoURI)
	cancel()
	db.CloseDB(ctx)
	db.CloseDB(ctx)
	t.Errorf("did not panic")
}

func TestBadDecodeCursor(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	db := Instance(ctx, helpers.MongoURI)
	defer db.CloseDB(ctx)
	c := &BadDecoderCursor{}
	_, err := db.getExpensesFromCur(ctx, c)
	helpers.ExpectError(t, err)
}

func TestBadEndDecodeCursor(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	db := Instance(ctx, helpers.MongoURI)
	defer db.CloseDB(ctx)
	c := &EndDecodeErrCursor{}
	_, err := db.getExpensesFromCur(ctx, c)
	helpers.ExpectError(t, err)
}

func TestCloseBadCursor(_ *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	db := Instance(ctx, helpers.MongoURI)
	defer db.CloseDB(ctx)
	c := &BadDecoderCursor{}
	db.closeCursor(ctx, c)
}

func TestErrWhileGetting(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	db := Instance(ctx, helpers.MongoURI)
	defer db.CloseDB(ctx)

	ctx2, cancel := context.WithCancel(context.Background())
	cancel()
	exp, err := db.GetExpenses(ctx2)
	helpers.ExpectError(t, err)
	helpers.ExpectEquals(t, &group.Expenses{}, exp)
}

func TestDB(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	db := Instance(ctx, helpers.MongoURI)
	defer db.CloseDB(ctx)
	t.Run("TestGetEmptyExpenses", func(t *testing.T) {
		helpers.FailTestIfErr(t, db.DropDB(ctx))
		expenses, err := db.GetExpenses(ctx)
		helpers.FailTestIfErr(t, err)
		helpers.ExpectEquals(t, expenses, &group.Expenses{})
		err = db.DropDB(ctx)
		helpers.FailTestIfErr(t, err)
	})
	t.Run("TestWriteEmptyExpense", func(t *testing.T) {
		helpers.FailTestIfErr(t, db.DropDB(ctx))
		err := db.WriteExpenses(ctx, &group.Expenses{})
		helpers.ExpectError(t, err)
		err = db.DropDB(ctx)
		helpers.FailTestIfErr(t, err)
	})
	t.Run("TestWriteNonEmptyExpense", func(t *testing.T) {
		helpers.FailTestIfErr(t, db.DropDB(ctx))
		err := db.WriteExpenses(ctx, &group.Expenses{Classified: []*group.Expense{group.NewTestExpense(t)}})
		helpers.FailTestIfErr(t, err)
		err = db.DropDB(ctx)
		helpers.FailTestIfErr(t, err)
	})
	t.Run("TestGetNonEmptyExpense", func(t *testing.T) {
		helpers.FailTestIfErr(t, db.DropDB(ctx))
		e := group.NewTestExpense(t)
		err := db.WriteExpenses(ctx, &group.Expenses{Classified: []*group.Expense{e}})
		helpers.FailTestIfErr(t, err)
		_, err = db.GetExpenses(ctx)
		helpers.FailTestIfErr(t, err)
		err = db.DropDB(ctx)
		helpers.FailTestIfErr(t, err)
	})
	t.Run("TestWriteExpenses", func(t *testing.T) {
		helpers.FailTestIfErr(t, db.DropDB(ctx))
		err := db.WriteExpenses(ctx, group.NewTestExpenses(t))
		helpers.FailTestIfErr(t, err)
		err = db.DropDB(ctx)
		helpers.FailTestIfErr(t, err)
	})
	t.Run("TestGetExpenses", func(t *testing.T) {
		helpers.FailTestIfErr(t, db.DropDB(ctx))
		exps := group.NewTestExpenses(t)
		err := db.WriteExpenses(ctx, exps)
		helpers.FailTestIfErr(t, err)
		_, err = db.GetExpenses(ctx)
		helpers.FailTestIfErr(t, err)
		err = db.DropDB(ctx)
		helpers.FailTestIfErr(t, err)
	})
}
