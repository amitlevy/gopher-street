package config

import "gitlab.com/amitlevy/gopher-street/read"

func NewTestConfig() *Data {
	return &Data{
		Files: map[string]File{
			"multiple-rows": {
				Cards: map[string]Card{
					"card1": {
						RowSubSetter: &read.RowSubSetter{
							Start: 1,
							End:   4,
						},
						ColMapper: &read.ColMapper{
							Date:        0,
							Description: 1,
							Credit:      4,
							// Refund:      5,
							// Balance:     6,
						},
						DateLayout: "02.01.2006",
					},
				},
			},
		},
	}
}
