package config

import (
	"path/filepath"
	"testing"

	"gitlab.com/amitlevy/gopher-street/helpers"
	"gitlab.com/amitlevy/gopher-street/read"
)

func TestConfigNotFound(t *testing.T) {
	err := InitConfig("non_exist_config.yml")
	helpers.ExpectError(t, err)
}

func TestCLIInitNoOptionalVars(t *testing.T) {
	err := InitConfig(filepath.Join("fixtures", "configs", "without-refund-balance.yml"))
	helpers.FailTestIfErr(t, err)
}

func TestCLIInitBadConfig(t *testing.T) {
	err := InitConfig(filepath.Join("fixtures", "configs", "bad.yml"))
	helpers.ExpectError(t, err)
}

func TestConfig(t *testing.T) {
	err := InitConfig(filepath.Join("fixtures", "configs", "config.yml"))
	helpers.FailTestIfErr(t, err)
	data := GetConfigData()
	helpers.ExpectEquals(t, data, &Data{
		Files: Files{
			"data": {Cards: FileCards{
				"card1": {
					RowSubSetter: &read.RowSubSetter{Start: 1, End: 4},
					ColMapper:    &read.ColMapper{Description: 1, Credit: 4, Refund: 5, Balance: 6},
					DateLayout:   "02.01.2006",
				},
			}},
			"test": {Cards: FileCards{
				"card1": {
					RowSubSetter: &read.RowSubSetter{Start: 13, End: 59},
					ColMapper:    &read.ColMapper{Description: 1, Credit: 4, Refund: 5, Balance: 6},
					DateLayout:   "02/01/2006",
				},
			}},
		},
		Classes: Classes{
			"eating outside": {"^pizza"},
			"living":         {"for rent"},
		},
		Tags:     Tags{"living": {"Crucial"}},
		Database: Database{URI: "mongodb://localhost"},
	})
}
