package config

import (
	"github.com/spf13/viper"
	"gitlab.com/amitlevy/gopher-street/read"
)

type Data struct {
	Files    Files
	Classes  Classes
	Tags     Tags
	Database Database
	Report   ReportConf
}

type Files map[string]File

type File struct {
	Cards FileCards
}

type FileCards map[string]Card

type Card struct {
	RowSubSetter *read.RowSubSetter
	ColMapper    *read.ColMapper
	DateLayout   string
}

type Classes map[string][]string
type Tags map[string][]string

type Database struct {
	URI string
}

type ReportConf struct {
	RightToLeftLanguage bool
	Headers             *struct {
		Date   string
		Amount string
		Class  string
		Tags   string
	}
}

func InitConfig(path string) error {
	viper.SetConfigFile(path)
	err := viper.ReadInConfig()
	return err
}

func GetConfigData() *Data {
	configData := &Data{}
	err := viper.Unmarshal(&configData)
	if err != nil {
		panic(err)
	}
	return configData
}
