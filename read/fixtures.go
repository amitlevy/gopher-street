package read

import (
	"testing"

	"gitlab.com/amitlevy/gopher-street/helpers"
)

func NewTestTransaction(t *testing.T, description string) *Transaction {
	return &Transaction{
		Date:        helpers.UTCDate(t, 2021, 03, 18),
		Description: description,
		Credit:      5.0,
		Refund:      0.0,
		Balance:     150.0,
	}
}

func NewTestCardTransactions(t *testing.T, fixturePath string) *CardTransactions {
	data := helpers.ReadCSVFixture(t, fixturePath)
	mapper := &ColMapper{
		Date:        0,
		Description: 1,
		Credit:      4,
		Refund:      5,
		Balance:     6,
	}
	layout := "02.01.2006"
	return NewCardTransactions(data, mapper, &RowSubSetter{}, layout)
}
