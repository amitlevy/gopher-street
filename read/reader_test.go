package read

import (
	"path/filepath"
	"testing"

	"gitlab.com/amitlevy/gopher-street/helpers"
)

func TestFactoryUnsupportedReader(t *testing.T) {
	_, err := ReaderFactory("non-supported")
	helpers.ExpectError(t, err)
}

func TestCSVReaderEmpty(t *testing.T) {
	r, err := ReaderFactory(".csv")
	helpers.FailTestIfErr(t, err)
	_, err = r.Read(filepath.Join(helpers.CSVTransactionsPath, "empty.csv"), false)
	helpers.ExpectError(t, err)
}

func TestCSVReaderNotExist(t *testing.T) {
	r, err := ReaderFactory(".csv")
	helpers.FailTestIfErr(t, err)
	_, err = r.Read(filepath.Join(helpers.CSVTransactionsPath, "not-exist.csv"), false)
	helpers.ExpectError(t, err)
}

func TestCSVReaderNotCSV(t *testing.T) {
	r, err := ReaderFactory(".csv")
	helpers.FailTestIfErr(t, err)
	_, err = r.Read(filepath.Join(helpers.CSVTransactionsPath, "bad-csv.csv"), false)
	helpers.ExpectError(t, err)
}

func TestCSVReaderSingleLine(t *testing.T) {
	r, err := ReaderFactory(".csv")
	helpers.FailTestIfErr(t, err)
	_, err = r.Read(filepath.Join(helpers.CSVTransactionsPath, "bad-not-matching-num-commas.csv"), false)
	helpers.ExpectError(t, err)
}

func TestCSVReader(t *testing.T) {
	r, err := ReaderFactory(".csv")
	helpers.FailTestIfErr(t, err)
	_, err = r.Read(filepath.Join(helpers.CSVTransactionsPath, "data.csv"), false)
	helpers.FailTestIfErr(t, err)
}

func TestXLSXReaderNotXLSX(t *testing.T) {
	r, err := ReaderFactory(".xlsx")
	helpers.FailTestIfErr(t, err)
	_, err = r.Read(filepath.Join(helpers.CSVTransactionsPath, "bad-csv.csv"), false)
	helpers.ExpectError(t, err)
}

func TestXLSXReader(t *testing.T) {
	r, err := ReaderFactory(".xlsx")
	helpers.FailTestIfErr(t, err)
	_, err = r.Read(filepath.Join(helpers.XLSXTransactionsPath, "empty_2_sheets.xlsx"), false)
	helpers.FailTestIfErr(t, err)
}
