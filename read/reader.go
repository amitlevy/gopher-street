package read

import (
	"encoding/csv"
	"fmt"
	"os"

	"github.com/tealeg/xlsx"
)

type Reader interface {
	Read(string, bool) ([][]string, error)
}

type XLSXReader struct{}
type CSVReader struct{}

func ReaderFactory(fileExtension string) (Reader, error) {
	switch fileExtension {
	case ".xlsx":
		return &XLSXReader{}, nil
	case ".csv":
		return &CSVReader{}, nil
	default:
		return nil, fmt.Errorf("unsupported file extension %s", fileExtension)
	}
}

func (r *XLSXReader) Read(filePath string, _ bool) ([][]string, error) {
	s, err := xlsx.FileToSlice(filePath)
	if err != nil {
		return [][]string{}, err
	}
	return s[0], err
}

func (r *CSVReader) Read(fileName string, readHeaders bool) ([][]string, error) {
	f, err := os.Open(fileName)
	if err != nil {
		return [][]string{}, err
	}
	defer f.Close()

	reader := csv.NewReader(f)
	if !readHeaders {
		// skip first line
		if _, err := reader.Read(); err != nil {
			return [][]string{}, err
		}
	}

	records, err := reader.ReadAll()
	if err != nil {
		return [][]string{}, err
	}

	return records, nil
}
