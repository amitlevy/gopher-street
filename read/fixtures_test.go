/*
This file is for test coverage purpose only.
As some packages use fixtures some here, they are not cross detected
*/
package read

import (
	"path/filepath"
	"testing"

	"gitlab.com/amitlevy/gopher-street/helpers"
)

func TestTransaction(t *testing.T) {
	NewTestTransaction(t, "test")
}

func TestCardTransactions(t *testing.T) {
	NewTestCardTransactions(t, filepath.Join(helpers.CSVTransactionsPath, "data.csv"))
}
