package main

import (
	"context"
	"errors"
	"fmt"
	"os"
	"sync"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/amitlevy/gopher-street/config"
	"gitlab.com/amitlevy/gopher-street/db"
	"gitlab.com/amitlevy/gopher-street/export"
	"gitlab.com/amitlevy/gopher-street/group"
)

var rootCmd = &cobra.Command{
	Use:   "gst",
	Short: "",
	Long:  ``,
}
var loadCmd = &cobra.Command{
	Use:   "load [file path]",
	Short: "Load data from file",
	Long:  ``,
	Args:  validateFilePath,
	RunE:  loadFile,
}
var getCmd = &cobra.Command{
	Use:   "get",
	Short: "Get expenses from DB",
	Long:  ``,
	RunE:  expenses,
}

var exportCmd = &cobra.Command{
	Use:   "export",
	Short: "Export expenses to file",
	Long:  ``,
	RunE:  exportExpenses,
}

var locker = sync.Once{}

func validateFilePath(_ *cobra.Command, args []string) error {
	if len(args) != 1 {
		return errors.New("File path missing")
	}
	stat, err := os.Stat(args[0])
	if os.IsNotExist(err) {
		return fmt.Errorf("No such file: %s", args[0])
	}
	if !stat.Mode().IsRegular() {
		return fmt.Errorf("Path is not file: %s", args[0])
	}
	return nil
}

func CLIInit(configPath string) error {
	if err := config.InitConfig(configPath); err != nil {
		return err
	}
	locker.Do(func() {
		rootCmd.AddCommand(loadCmd)
		rootCmd.AddCommand(getCmd)
		rootCmd.AddCommand(exportCmd)
		exportCmd.PersistentFlags().String("format", "csv", "The encoding of the exported file")
		exportCmd.PersistentFlags().String("path", "exported", "The path for the file to be created excluding extension")
	})

	return nil
}

func loadFile(cmd *cobra.Command, args []string) error {
	conf := config.GetConfigData()
	expenses, err := group.ExpensesFromFile(conf, args[0])
	if err != nil {
		return err
	}
	if len(expenses.ToSlice()) > 0 {
		if err := writeExpensesToDB(conf, expenses); err != nil {
			return err
		}
	}
	cmd.Println()
	writeReport(cmd, expenses)
	cmd.Println()
	cmd.Println("Done!")

	return nil
}

func exportExpenses(cmd *cobra.Command, _ []string) error {
	format, _ := cmd.Flags().GetString("format")
	filePath, _ := cmd.Flags().GetString("path")
	exporter, err := export.ExporterFactory(fmt.Sprintf(".%s", format))
	if err != nil {
		return err
	}
	expenses, err := expensesFromDB()
	if err != nil {
		return err
	}
	return exporter.Export(expenses, filePath)
}

func writeExpensesToDB(conf *config.Data, expenses *group.Expenses) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	_db := db.Instance(ctx, conf.Database.URI)
	defer _db.CloseDB(ctx)

	return _db.WriteExpenses(ctx, expenses)
}

func writeReport(cmd *cobra.Command, expenses *group.Expenses) {
	conf := config.GetConfigData()
	r := export.Reporter{ReportConf: conf.Report}
	cmd.Println(r.Report(expenses))
}

func expenses(cmd *cobra.Command, _ []string) error {
	expenses, err := expensesFromDB()
	if err != nil {
		return err
	}
	writeReport(cmd, expenses)
	cmd.Printf("\n%d expenses found\n", len(expenses.ToSlice()))

	return nil
}

func expensesFromDB() (*group.Expenses, error) {
	conf := config.GetConfigData()
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	_db := db.Instance(ctx, conf.Database.URI)
	defer _db.CloseDB(ctx)

	return _db.GetExpenses(ctx)
}
