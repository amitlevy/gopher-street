package export

import (
	"encoding/csv"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/tealeg/xlsx"
	"gitlab.com/amitlevy/gopher-street/group"
)

type ExpensesExporter interface {
	Export(expenses *group.Expenses, filePath string) error
}

type CSVExporter struct{}
type XLSXExporter struct{}

type ExpenseToExport struct {
	Date   time.Time
	Amount float64
	Class  string
	Tags   string
}

func ExporterFactory(fileExtension string) (ExpensesExporter, error) {
	switch fileExtension {
	case ".xlsx":
		return &XLSXExporter{}, nil
	case ".csv":
		return &CSVExporter{}, nil
	default:
		return nil, fmt.Errorf("unsupported file extension %s", fileExtension)
	}
}

func (exporter *XLSXExporter) Export(expenses *group.Expenses, filePath string) error {
	expensesToReport := expensesToReport(expenses.Classified)
	file := xlsx.NewFile()
	sheet, _ := file.AddSheet("Expenses")
	sheet.AddRow().WriteSlice(&[]string{"Date", "Amount", "Class", "Tags"}, -1)
	for _, record := range expensesToReport {
		formatted := formatRecord(record)
		sheet.AddRow().WriteSlice(&formatted, -1)
	}

	return file.Save(fmt.Sprintf("%s.xlsx", strings.TrimSuffix(filePath, filepath.Ext(filePath))))
}

func (exporter *CSVExporter) Export(expenses *group.Expenses, filePath string) error {
	file, err := os.Create(fmt.Sprintf("%s.csv", strings.TrimSuffix(filePath, filepath.Ext(filePath))))
	if err != nil {
		return err
	}
	defer file.Close()
	w := csv.NewWriter(file)
	data := [][]string{{"Date", "Amount", "Class", "Tags"}}
	for _, record := range expensesToReport(expenses.Classified) {
		data = append(data, formatRecord(record))
	}

	return w.WriteAll(data)
}

func formatRecord(record *ExpenseToExport) []string {
	return []string{record.Date.Format("2006-01-02T15:04"), fmt.Sprintf("%.2f", record.Amount), record.Class, record.Tags}
}

func expensesToReport(expenses []*group.Expense) []*ExpenseToExport {
	expensesToReport := make([]*ExpenseToExport, len(expenses))

	for i, exp := range expenses {
		expensesToReport[i] = &ExpenseToExport{
			Date:   exp.Date,
			Amount: exp.Amount,
			Class:  exp.Class,
			Tags:   strings.Join(exp.Tags, ","),
		}
	}
	return expensesToReport
}
