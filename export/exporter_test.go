package export

import (
	"fmt"
	"os"
	"path/filepath"
	"testing"

	"gitlab.com/amitlevy/gopher-street/group"
	"gitlab.com/amitlevy/gopher-street/helpers"
	"gitlab.com/amitlevy/gopher-street/read"
)

const (
	XLSX string = "xlsx"
	CSV  string = "csv"
)

func TestExporter(t *testing.T) {
	testCases := []struct {
		desc     string
		format   string
		expenses *group.Expenses
		expected [][]string
	}{
		{
			desc:     "EmptyExpensesToEmptyFile",
			expenses: &group.Expenses{},
			expected: [][]string{{"Date", "Amount", "Class", "Tags"}},
		},
		{
			desc:     "SingleExpense",
			expenses: &group.Expenses{Classified: []*group.Expense{group.NewTestExpense(t)}},
			expected: [][]string{
				{"Date", "Amount", "Class", "Tags"}, {"2021-03-18T00:00", "5.00", "class1", "tag1"},
			},
		},
		{
			desc:     "ManyExpenses",
			expenses: group.NewTestExpenses(t),
			expected: [][]string{
				{"Date", "Amount", "Class", "Tags"}, {"2021-03-18T00:00", "5.00", "class1", "tag1"},
				{"2021-04-19T00:00", "5.00", "class1", "tag2"},
				{"2021-05-20T00:00", "5.00", "class2", "tag1"},
			},
		},
		{
			desc: "ExpenseWithManyTags",
			expenses: &group.Expenses{Classified: []*group.Expense{{
				Date:   helpers.UTCDate(t, 2022, 9, 24),
				Amount: 5,
				Class:  "class",
				Tags:   []group.Tag{"tag1", "tag2", "tag3"},
			}}},
			expected: [][]string{
				{"Date", "Amount", "Class", "Tags"},
				{"2022-09-24T00:00", "5.00", "class", "tag1,tag2,tag3"},
			},
		},
	}
	for _, tC := range testCases {
		for _, format := range []string{XLSX, CSV} {
			t.Run(fmt.Sprintf("%s-%s", format, tC.desc), func(t *testing.T) {
				filename := fmt.Sprintf("%s.%s", tC.desc, format)
				exporter, err := ExporterFactory(filepath.Ext(filename))
				helpers.FailTestIfErr(t, err)
				err = exporter.Export(tC.expenses, filename)
				helpers.FailTestIfErr(t, err)
				reader, err := read.ReaderFactory(filepath.Ext(filename))
				helpers.FailTestIfErr(t, err)
				content, err := reader.Read(filename, true)
				helpers.FailTestIfErr(t, err)
				helpers.ExpectEquals(t, content, tC.expected)
				os.Remove(filename)
			})
		}
	}
}

func TestUnsupportedExportType(t *testing.T) {
	_, err := ExporterFactory(".unsupported")
	helpers.ExpectError(t, err)
}
