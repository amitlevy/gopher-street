package export

import (
	"testing"
	"time"

	"gitlab.com/amitlevy/gopher-street/group"

	"gitlab.com/amitlevy/gopher-street/helpers"
)

func TestReportTable(t *testing.T) {
	testCases := []struct {
		desc     string
		expenses *group.Expenses
	}{
		{
			desc:     "Empty table for empty expenses",
			expenses: &group.Expenses{},
		},
		{
			desc: "Single row for single expense",
			expenses: &group.Expenses{Classified: []*group.Expense{
				{
					Date:   helpers.UTCDate(t, 2020, time.April, 24),
					Amount: 53.6,
					Class:  "Food Outside",
					Tags:   []group.Tag{group.Crucial},
				},
			}, Unclassified: []*group.Expense{}},
		},
		{
			desc: "Multiple rows with total for many expense",
			expenses: &group.Expenses{Classified: []*group.Expense{
				{
					Date:   helpers.UTCDate(t, 2020, time.April, 24),
					Amount: 53.6,
					Class:  "Food Outside",
					Tags:   []group.Tag{group.Crucial},
				},
				{
					Date:   helpers.UTCDate(t, 2020, time.April, 24),
					Amount: 26.4,
					Class:  "Food Outside",
					Tags:   []group.Tag{group.Crucial},
				},
				{
					Date:   helpers.UTCDate(t, 2020, time.April, 24),
					Amount: 30.0,
					Class:  "Food Outside",
					Tags:   []group.Tag{group.Crucial},
				},
				{
					Date:   helpers.UTCDate(t, 2020, time.April, 24),
					Amount: 30.0,
					Class:  "Food Outside",
					Tags:   []group.Tag{group.Crucial, group.Recurring},
				},
			}},
		},
	}
	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			r := Reporter{}
			fixture := "reports"
			actual := r.Report(tC.expenses)
			helpers.CheckUpdateFlag(t, fixture, actual)
			helpers.ExpectEqualsGolden(t, fixture, actual)
		})
	}
}
