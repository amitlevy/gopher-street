/*
This file is for test coverage purpose only.
As some packages use fixtures some here, they are not cross detected
*/
package group

import (
	"testing"
)

func TestExpense(t *testing.T) {
	NewTestExpense(t)
}

func TestExpenses(t *testing.T) {
	NewTestExpenses(t)
}

func TestClassifier(_ *testing.T) {
	NewTestClassifier()
}

func TestClassifierWithData(_ *testing.T) {
	NewTestClassifierWithData()
}

func TestTagger(_ *testing.T) {
	NewTestTagger()
}

func TestTaggerWithData(_ *testing.T) {
	NewTestTaggerWithData()
}
