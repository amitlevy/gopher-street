package group

import (
	"testing"

	"gitlab.com/amitlevy/gopher-street/helpers"
)

func TestTag(t *testing.T) {
	testCases := []struct {
		desc         string
		tagger       Tagger
		class        string
		expectedTags []Tag
	}{
		{
			desc:         "EmptyTagWhenEmptyClass",
			expectedTags: []Tag{},
		},
		{
			desc:         "NoTagIfNoMatch",
			class:        "class",
			expectedTags: []Tag{},
		},
		{
			desc:         "TagAccordingToDict",
			tagger:       Tagger{map[string][]Tag{"class": {"tag"}}},
			class:        "class",
			expectedTags: []Tag{"tag"},
		},
		{
			desc:         "TagAccordingToRegexInDict",
			tagger:       Tagger{map[string][]Tag{"^c.*s$": {"tag"}}},
			class:        "class",
			expectedTags: []Tag{"tag"},
		},
	}
	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			tags := tC.tagger.Tags(tC.class)
			helpers.ExpectEquals(t, tags, tC.expectedTags)
		})
	}
}
