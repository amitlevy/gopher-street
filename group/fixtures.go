package group

import (
	"testing"

	"gitlab.com/amitlevy/gopher-street/helpers"
)

func NewTestExpense(t *testing.T) *Expense {
	return &Expense{
		Date:   helpers.UTCDate(t, 2021, 03, 18),
		Amount: 5.0,
		Class:  "class1",
		Tags:   []Tag{"tag1"},
	}
}

func NewTestExpenses(t *testing.T) *Expenses {
	return &Expenses{Classified: []*Expense{
		{
			Date:   helpers.UTCDate(t, 2021, 03, 18),
			Amount: 5.0,
			Class:  "class1",
			Tags:   []Tag{"tag1"},
		},
		{
			Date:   helpers.UTCDate(t, 2021, 04, 19),
			Amount: 5.0,
			Class:  "class1",
			Tags:   []Tag{"tag2"},
		},
		{
			Date:   helpers.UTCDate(t, 2021, 05, 20),
			Amount: 5.0,
			Class:  "class2",
			Tags:   []Tag{"tag1"},
		},
	}}
}

func NewTestClassifier() *Classifier {
	return &Classifier{map[string]string{
		"description1": "class1",
		"^d.*1$":       "class1",
		"description2": "class2",
	}}
}

func NewTestClassifierWithData() *Classifier {
	return &Classifier{map[string]string{
		"^pizza":   "Eating outside",
		"for rent": "Living",
	}}
}

func NewTestTagger() *Tagger {
	return &Tagger{map[string][]Tag{
		"class1": {"tag1", "tag2"},
		"class2": {"tag3"},
		"^c.*3$": {"tag4"},
	}}
}

func NewTestTaggerWithData() *Tagger {
	return &Tagger{map[string][]Tag{
		"Living": {Crucial},
	}}
}
