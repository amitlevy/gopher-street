package group

import (
	"testing"

	"gitlab.com/amitlevy/gopher-street/helpers"
)

func TestClass(t *testing.T) {
	testCases := []struct {
		desc            string
		classifier      Classifier
		transactionDesc string
		expectedClass   string
		expectError     bool
	}{
		{
			desc:        "EmptyClassWhenEmptyDescription",
			expectError: true,
		},
		{
			desc:            "DescriptionAsClassIfNoMatch",
			transactionDesc: "description",
			expectedClass:   "description",
			expectError:     true,
		},
		{
			desc:            "ClassAccordingToDict",
			classifier:      Classifier{map[string]string{"description": "class"}},
			transactionDesc: "description",
			expectedClass:   "class",
		},
		{
			desc:            "ClassAccordingToRegexInDict",
			classifier:      Classifier{map[string]string{"^d.*n$": "class"}},
			transactionDesc: "description",
			expectedClass:   "class",
		},
	}
	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			class, err := tC.classifier.Class(tC.transactionDesc)
			if class != tC.expectedClass {
				t.Errorf("expected class %s received %s", tC.expectedClass, class)
			}
			if err != nil && !tC.expectError {
				helpers.FailTestIfErr(t, err)
			}
		})
	}
}

func TestNewClassifier(t *testing.T) {
	classesToDescriptions := map[string][]string{
		"Eating outside": {"^pizza"},
		"Living":         {"for rent"},
	}
	descriptionToClasses := map[string]string{
		"^pizza":   "Eating outside",
		"for rent": "Living",
	}
	cl := NewClassifier(classesToDescriptions)
	helpers.ExpectEquals(t, cl.descriptionToClass, descriptionToClasses)
}
