package group

import (
	"path/filepath"
	"testing"
	"time"

	"gitlab.com/amitlevy/gopher-street/config"
	"gitlab.com/amitlevy/gopher-street/helpers"
	"gitlab.com/amitlevy/gopher-street/read"
)

func TestExpenseCreation(t *testing.T) {
	testCases := []struct {
		desc         string
		transactions []read.Transaction
		classifier   *Classifier
		tagger       *Tagger
		expected     *Expenses
	}{
		{
			desc:         "From empty data -> empty expenses",
			transactions: []read.Transaction{},
			classifier:   &Classifier{},
			tagger:       &Tagger{},
			expected:     &Expenses{Classified: []*Expense{}, Unclassified: []*Expense{}},
		},
		{
			desc:         "From empty transactions -> empty expenses",
			transactions: []read.Transaction{},
			classifier:   NewTestClassifier(),
			tagger:       &Tagger{},
			expected:     NewEmptyExpenses(),
		},
		{
			desc:         "From non-matching classes -> all expenses. Class same as description",
			transactions: []read.Transaction{*read.NewTestTransaction(t, "pizza")},
			classifier:   NewTestClassifier(),
			tagger:       &Tagger{},
			expected: &Expenses{Unclassified: []*Expense{{
				Date:   helpers.UTCDate(t, 2021, time.March, 18),
				Amount: 5.0,
				Class:  "pizza",
				Tags:   []Tag{}},
			}, Classified: []*Expense{}},
		},
		{
			desc:         "From matching classes -> all expenses. Class set by classifier",
			transactions: []read.Transaction{*read.NewTestTransaction(t, "description1")},
			classifier:   NewTestClassifier(),
			tagger:       &Tagger{},
			expected: &Expenses{Classified: []*Expense{{
				Date:   helpers.UTCDate(t, 2021, time.March, 18),
				Amount: 5.0,
				Class:  "class1",
				Tags:   []Tag{}},
			}, Unclassified: []*Expense{}},
		},
		{
			desc:         "From non-matching tags -> all expenses. Class set by classifier",
			transactions: []read.Transaction{*read.NewTestTransaction(t, "description1")},
			classifier:   NewTestClassifier(),
			tagger:       &Tagger{classesToTags: map[string][]Tag{"nonExistClass": {"someTag"}}},
			expected: &Expenses{Classified: []*Expense{
				{
					Date:   helpers.UTCDate(t, 2021, time.March, 18),
					Amount: 5.0,
					Class:  "class1",
					Tags:   []Tag{},
				},
			}, Unclassified: []*Expense{}},
		},
		{
			desc:         "From matching tags and classes -> all expenses. Class set by classifier, if tags match they should show",
			transactions: []read.Transaction{*read.NewTestTransaction(t, "description1")},
			classifier:   NewTestClassifier(),
			tagger:       NewTestTagger(),
			expected: &Expenses{Classified: []*Expense{{
				Date:   helpers.UTCDate(t, 2021, time.March, 18),
				Amount: 5.0,
				Class:  "class1",
				Tags:   []Tag{"tag1", "tag2"}},
			}, Unclassified: []*Expense{}},
		},
	}
	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			expense := NewExpenses(tC.transactions, tC.classifier, tC.tagger)
			helpers.ExpectEquals(t, expense, tC.expected)
		})
	}
}

func TestGetExpensesBadFile(t *testing.T) {
	_, err := ExpensesFromFile(&config.Data{}, "unsupported.json")
	helpers.ExpectError(t, err)
}

func TestGetExpensesNonExist(t *testing.T) {
	_, err := ExpensesFromFile(&config.Data{}, "non-exist.csv")
	helpers.ExpectError(t, err)
}

func TestGetExpensesFileDoNotAppearInConfig(t *testing.T) {
	file := filepath.Join("..", "read", helpers.CSVTransactionsPath, "with-refund.csv")
	_, err := ExpensesFromFile(config.NewTestConfig(), file)
	helpers.ExpectError(t, err)
}

func TestGetExpensesFromFile(t *testing.T) {
	file := filepath.Join("..", "read", helpers.CSVTransactionsPath, "multiple-rows.csv")
	exps, err := ExpensesFromFile(config.NewTestConfig(), file)
	helpers.FailTestIfErr(t, err)
	helpers.ExpectEquals(t, exps.ToSlice(), []*Expense{
		{
			Date:   helpers.UTCDate(t, 2021, time.March, 18),
			Amount: 5.0,
			Class:  "pizza1",
			Tags:   []string{},
		},
		{
			Date:   helpers.UTCDate(t, 2021, time.March, 18),
			Amount: 5.0,
			Class:  "pizza2",
			Tags:   []string{},
		},
		{
			Date:   helpers.UTCDate(t, 2021, time.March, 18),
			Amount: 5.0,
			Class:  "pizza3",
			Tags:   []string{},
		},
	})
}

func TestGroupByDate(t *testing.T) {
	cl := NewTestClassifierWithData()
	ct := read.NewTestCardTransactions(t, filepath.Join("..", "read", helpers.CSVTransactionsPath, "data.csv"))
	tagger := NewTestTaggerWithData()
	trs, err := ct.Transactions()
	helpers.FailTestIfErr(t, err)
	expense := NewExpenses(trs, cl, tagger)
	byMonth := expense.GroupByMonth()
	helpers.ExpectEquals(t, byMonth, map[time.Month][]Expense{
		time.March: {
			{
				Date:   helpers.UTCDate(t, 2021, time.March, 18),
				Class:  "Eating outside",
				Amount: 5,
				Tags:   []Tag{},
			},
			{
				Date:   helpers.UTCDate(t, 2021, time.March, 22),
				Class:  "shirt",
				Amount: 20,
				Tags:   []Tag{},
			},
		},
		time.April: {
			{
				Date:   helpers.UTCDate(t, 2021, time.April, 24),
				Class:  "Living",
				Amount: 3500,
				Tags:   []Tag{"Crucial"},
			},
		},
		time.May: {
			{
				Date:   helpers.UTCDate(t, 2021, time.May, 5),
				Class:  "Eating outside",
				Amount: 5,
				Tags:   []Tag{},
			},
		},
	})
}

func TestGroupByClass(t *testing.T) {
	cl := NewTestClassifierWithData()
	ct := read.NewTestCardTransactions(t, filepath.Join("..", "read", helpers.CSVTransactionsPath, "data.csv"))
	tagger := NewTestTaggerWithData()
	trs, err := ct.Transactions()
	helpers.FailTestIfErr(t, err)
	expense := NewExpenses(trs, cl, tagger)
	byMonth := expense.GroupByClass()
	helpers.ExpectEquals(t, byMonth, map[string][]Expense{
		"Eating outside": {
			{
				Date:   helpers.UTCDate(t, 2021, time.March, 18),
				Class:  "Eating outside",
				Amount: 5,
				Tags:   []Tag{},
			},
			{
				Date:   helpers.UTCDate(t, 2021, time.May, 5),
				Class:  "Eating outside",
				Amount: 5,
				Tags:   []Tag{},
			},
		},
		"shirt": {
			{
				Date:   helpers.UTCDate(t, 2021, time.March, 22),
				Class:  "shirt",
				Amount: 20,
				Tags:   []Tag{},
			},
		},
		"Living": {
			{
				Date:   helpers.UTCDate(t, 2021, time.April, 24),
				Class:  "Living",
				Amount: 3500,
				Tags:   []Tag{"Crucial"},
			},
		},
	})
}

func TestGroupByTag(t *testing.T) {
	cl := NewTestClassifierWithData()
	ct := read.NewTestCardTransactions(t, filepath.Join("..", "read", helpers.CSVTransactionsPath, "data.csv"))
	tagger := NewTestTaggerWithData()
	trs, err := ct.Transactions()
	helpers.FailTestIfErr(t, err)
	expense := NewExpenses(trs, cl, tagger)
	byMonth := expense.GroupByTag()
	helpers.ExpectEquals(t, byMonth, map[Tag][]Expense{
		"Crucial": {
			{
				Date:   helpers.UTCDate(t, 2021, time.April, 24),
				Class:  "Living",
				Amount: 3500,
				Tags:   []Tag{"Crucial"},
			},
		},
		"None": {
			{
				Date:   helpers.UTCDate(t, 2021, time.March, 18),
				Class:  "Eating outside",
				Amount: 5,
				Tags:   []Tag{},
			},
			{
				Date:   helpers.UTCDate(t, 2021, time.March, 22),
				Class:  "shirt",
				Amount: 20,
				Tags:   []Tag{},
			},
			{
				Date:   helpers.UTCDate(t, 2021, time.May, 5),
				Class:  "Eating outside",
				Amount: 5,
				Tags:   []Tag{},
			},
		},
	})
}
